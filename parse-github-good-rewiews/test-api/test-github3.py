from entity.pullrequest import PullRequest


def get_pr_history(username: str, repo_name: str, pr_number: int):
    pr = PullRequest(username, repo_name, pr_number)
    pr.build_history()


if __name__ == '__main__':
    get_pr_history("AlexVanGogen", "toy-repo", 1)
