class Commit:

    def __init__(self, commit) -> None:
        self.sha = commit.sha
        self.author = commit.author.login
        self.message = commit.message
        self.created_at = commit.commit.committer['date']
        self.diff = commit.diff()

    def __str__(self):
        return "{:30} new commit\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n"\
            .format(
                self.created_at,
                "sha", self.sha,
                "author", self.author,
                "message", self.message,
                "diff", self.diff
            )
