class Comment:

    def __init__(self, comment) -> None:
        self.author = comment.user.login
        self.text = comment.body_text
        self.created_at = self.__standardize_date(comment.created_at)
        self.commit_id = comment.commit_id
        self.diff = comment.diff_hunk
        self.short_diff = self.__fetch_main_line_from_code_comment(self.diff)

    def __str__(self):
        return "{:30} new comment\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n" \
               "\t{:10}{}\n" \
            .format(
                self.created_at,
                "commit_id", self.commit_id,
                "author", self.author,
                "text", self.text,
                "diff", self.short_diff
            )

    def __standardize_date(self, date):
        return "{}-{}-{}T{}:{}:{}Z".format(
            date.year,
            # ('0' if len(date.month) == 1 else '') + date.month,
            # ('0' if len(date.day) == 1 else '') + date.day,
            # ('0' if len(date.hour) == 1 else '') + date.hour,
            # ('0' if len(date.minute) == 1 else '') + date.minute,
            # ('0' if len(date.second) == 1 else '') + date.second
            ('0' if date.month // 10 == 0 else '') + str(date.month),
            ('0' if date.day // 10 == 0 else '') + str(date.day),
            ('0' if date.hour // 10 == 0 else '') + str(date.hour),
            ('0' if date.minute // 10 == 0 else '') + str(date.minute),
            ('0' if date.second // 10 == 0 else '') + str(date.second)
        )

    def __fetch_main_line_from_code_comment(self, diff):
        return diff.split("\n+    ")[-1]
