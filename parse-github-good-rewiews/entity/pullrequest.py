import github3 as gh
import requests

from .commit import Commit
from .comment import Comment
from collections import OrderedDict


class PullRequest:

    def __init__(self, repo_owner, repo_name, pr_number) -> None:
        self.repo_owner = repo_owner
        self.repo_name = repo_name
        self.pr_number = pr_number
        self.chronic_events = dict()

    def build_history(self) -> None:
        """
        Send GET request via github3 to get concrete pull request info
        :return: None, but events (commits and review comments) are collected and stored
        """
        pr_info = gh.pull_request(self.repo_owner, self.repo_name, self.pr_number)
        print("{:30} pull request created".format(str(pr_info.created_at)))
        events = dict()
        for next_commit in pr_info.commits():
            commit = Commit(next_commit)
            events[commit.created_at] = commit
        for next_comment in pr_info.review_comments():
            comment = Comment(next_comment)
            events[comment.created_at] = comment
        self.chronic_events = OrderedDict(sorted(events.items(), key=lambda p: p[0]))
